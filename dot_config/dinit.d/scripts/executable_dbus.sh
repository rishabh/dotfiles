#!/bin/sh

exec dbus-daemon --session --nofork --nopidfile --print-address="${DINIT_FD?:No ready fd}" --address="${DBUS_SESSION_BUS_ADDRESS:?No address}"
